# MySQL

## Introduction

This chart bootstraps a single node MySQL deployment on a
[Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh)
package manager.

It is almost completely taken from
[Kubernetes Charts project](https://github.com/kubernetes/charts).

## Prerequisites

- Kubernetes 1.4+ with Beta APIs enabled.
- PV provisioner support in the underlying infrastructure (if persistent
  storage is enabled).

## Installing the Chart

To install the chart with the release name MY_RELEASE:

    $ helm install --name MY_RELEASE stable/mysql

The command deploys MySQL on the Kubernetes cluster in the default
configuration. The [configuration](#configuration) section lists the parameters
that can be configured during installation.

A random password will be generated for the root user. You can retrieve the
root password by running the following command. Make sure to replace
MY_RELEASE:

    $ printf $(printf '\%o' `kubectl get secret MY_RELEASE-mysql -o jsonpath="{.data.mysql-root-password[*]}"`)

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

    $ helm delete my-release

The command removes all the Kubernetes components associated with the chart and
deletes the release.

## Configuration

The following tables lists the configurable parameters of the MySQL chart and
their default values.

| Parameter                  | Description                        | Default                                                    |
| -------------------------- | ---------------------------------- | ---------------------------------------------------------- |
| `mysqlUser`                | Username of new user to create.    | dbuser                                                     |
| `mysqlDatabase`            | Name for new database to create.   | sandbox                                                    |
| `persistence.enabled`      | Create a volume to store data      | false                                                      |
| `persistence.size`         | Size of persistent volume claim    | 8Gi RW                                                     |
| `persistence.storageClass` | Type of persistent volume claim    | generic                                                    |
| `persistence.accessMode`   | ReadWriteOnce or ReadOnly          | ReadWriteOnce                                              |
| `resources`                | CPU/Memory resource requests       | Memory: `256Mi`, CPU: `100m`                               |

Some of the parameters above map to the env variables defined in the
[MySQL DockerHub image](https://hub.docker.com/_/mysql/).

Specify each parameter using the `--set key=value[,key=value]` argument to
`helm install`. For example,

    $ helm install --name my-release \
        --set mysqlUser=my-user,mysqlDatabase=my-database \
        stable/mysql

The above command creates a standard database user named `my-user`, with a
generated password, who has access to a database named `my-database`.

Alternatively, a YAML file that specifies the values for the parameters can be
provided while installing the chart. For example,

    $ helm install --name my-release -f values.yaml stable/mysql

> **Tip**: You can use the default [values.yaml](values.yaml)

## Persistence

The [MySQL](https://hub.docker.com/_/mysql/) image stores the MySQL data and
configurations at the `/var/lib/mysql` path of the container.

By default an
[`EmptyDir` local volume](http://kubernetes.io/docs/user-guide/volumes/#emptydir).
is created and mounted to that directory. To use a PersistentVolumeClaim
instead change the values.yaml to enable persistence.
